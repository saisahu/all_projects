from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='Home'),
    path('find_sum', views.find_sum, name='Find_Sum'),
    path('find_sum/process_insert_to_database', views.insert_to_database, 
    name='Insert_to_Database'), 
]