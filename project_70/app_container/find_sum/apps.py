from django.apps import AppConfig


class FindSumConfig(AppConfig):
    name = 'find_sum'
