from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def home(request):
    return HttpResponse(
        'This is the home page of the app named - "find_sum".'
        'Now go to the location "www.domainname.com/find_sum" to use the app.'
    ) 


def find_sum(request):
    # return HttpResponse('This is the user interface page of find_sum.')
    return render(request, 'find_sum.html')


def insert_to_database(request):
    request_object = request.POST['Text']
    # Get the request text
    request_text = str(request_object)

    # Convert the request text to request dict, so that we can access the key
    # value pairs in a pythonic way.
    request_dict = eval(request_text)

    user_name = request_dict['user_name']
    first_number = request_dict['first_number']
    last_number = request_dict['last_number']

    # Inserting data to the database
    import mysql.connector as connector

    database_connection =  connector.connect(
        host = 'localhost',
        user = 'root',
        password = 'Python-SQL',
        database = 'PythonDatabase'
    )

    print(database_connection)

    my_cursor = database_connection.cursor()

    sql_statement = (
        'INSERT INTO find_sum (Name, First_Number, Last_Number) '
        'VALUES (%s, %s, %s)'
    )
    values = [
        (user_name, first_number, last_number),
    ]

    my_cursor.executemany(sql_statement, values)

    database_connection.commit()

    # Return a success message to the user. 
    return HttpResponse(
        'Your data has been successfully inserted to the database'
    )