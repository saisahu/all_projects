# %%
import mysql.connector as connector

database_connection =  connector.connect(
    host = 'localhost',
    user = 'root',
    password = 'Python-SQL',
    database = 'PythonDatabase'
)

print(database_connection)

my_cursor = database_connection.cursor()

sql_statement = ('CREATE TABLE find_sum (Name VARCHAR(255),' 
'First_Number VARCHAR(11), Last_Number VARCHAR(11))')

my_cursor.execute(sql_statement)