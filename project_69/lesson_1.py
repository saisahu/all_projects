# %%
"""This is to make some practice with MySQL and Python. 
"""

# 1. First install mysql-connector-python. The module that gives python the 
# ability to connect and deal with my mysql database. 

# 2. Download the whole community edition. 
# Now install
#   1. MySQL Server
#   2. MySQL Connector for python
#   3. MySQL Workbench 
#   4. MySQL Ducumentation 

# Configure the MySQL Server 
#   1. This includes configuring MySQL server as a windows process, 
#   2. The user name of the root account
#   3. Setting the password for the root account
#   4. Setting the port number of the MySQL Server

# Now the steps include in operating MySQL programatically through python are
#   1. Import the connector module from the mysql-connector-python package.
#   2. Use connect fuction to connect to the MySQL server and get the database
#      database connection object. 
#   3. Get the cursor object from the connection object. 
#   4. Use the execute() and executemany() method of the cursor object and pass
#      SQL statement (or SQL statement and values in case of executemany) to it 
#      to deal with databse server.
#   5. All the SQL statements can be found on w3schools website to do 
#      different things.  
#   5. Finally use the commit() method of the database connection object.
#   6. If you are getting some data from the database then use fetchall() 
#      method of the cursor object to get all the data that is collected from
#      the data base server. This data will be in the form of an iterable that 
#      contains rows as tuples. You can iterate upon it to get all the rows.    


import mysql.connector as connector

database_connection =  connector.connect(
    host = 'localhost',
    user = 'root',
    password = 'Python-SQL',
    database = 'PythonDatabase'
)

print(database_connection)

my_cursor = database_connection.cursor()

# ------------------------------------------------- Set of SQL Statements
# 1. sql_statement = 'CREATE DATABASE PythonDatabase'

# 2. sql_statement = ('CREATE TABLE Employees (Sl_No VARCHAR(11),' 
# 'Name VARCHAR(255), Age VARCHAR(11))')

# 3. sql_statement = (
#     'ALTER TABLE Employees ADD COLUMN DOB DATETIME'
# )

# 4. sql_statement = (
#     'INSERT INTO Employees (Sl_No, Name, Age, DOB) VALUES (%s, %s, %s, %s)'
# )
# values = [
#     (3, 'Dany', 23, '2020-12-01'),
# ]

# 5. sql_statement = (
#     'UPDATE Employees SET DOB = %s, Age = %s'
# )
# values = [
#     ('1997-11-06', 23),
#     ('1998-11-06', 22),
#     ('1999-11-06', 20)
# ]

sql_statement = (
    'SELECT Sl_No, Name, Age FROM Employees LIMIT 2'
)
# ------------------------------------------------- Set of SQL Statements

my_cursor.execute(sql_statement)
# my_cursor.executemany(sql_statement, values)

# database_connection.commit()

print('Execution done.')

results = my_cursor.fetchall()

for result in results:
    print(result)


# %%

# How to take advantages of Pandas 
# 1. Once you get the items from the MySQL server you can make it a pandas'
# data frame object and use pandas to analyse the data. This is the  
import pandas as pd 
data_frame = pd.DataFrame(
    results,
    columns = ['Sl_No', 'Name', 'Age']
)

# 2. Then you can save the piece of data to a csv file also so as to do 
# iterative processings flexibly and easily. 
display(data_frame)
data_frame.to_csv('employee.csv', index=False)

