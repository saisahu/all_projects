# %%
list_ = [1, 2, 3, 4]
print(hasattr(list_, '__iter__'))
print(hasattr(list_, '__next__'))

list_iterable = list_.__iter__()

print(list_iterable.__next__())
print(list_iterable.__next__())

for item in list_iterable:
    print(item)
