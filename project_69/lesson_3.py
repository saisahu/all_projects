# %%
"""This is to do practice with Numpy in Python.
"""
# Why to use numpy even when all the functionality provided by numpy can 
# be easily achieved by using python lists.
# The numpy module has an implimentation of 'C' at it's back. As 'C' is 
# on an average 45 times faster than Python and that's why Numpy arrays 
# also shows a 45 times faster execution rate. While doing any deep learing 
# projects you need to deal with huge amount of tensors. In that sictuation
# a tensor operation written using Numpy will be 45 times faster that is a 
# tremedous advantage. 

# What is needed to be mastered to be done using Numpy.
#   1. Making multidimensional arries of
#       - All zeros
#       - All ones
#       - All random
#       - All by hand assignment 
#   2. Get the shape of the array. 
#   3. Reshape the array. 
#   4. Accessing array parts. In numpy as the arraies are multidimensional, 
#      accessing the elements is going to be based on axes.
#   5. Update a part of the array.
#   6. Take different parts of the array, apply different mathematical 
#      operations on them, then reunite all the result parts. 

import numpy as np

# 1. Making multidimensional arraies of different kind. 
nda_all_zeros = np.zeros((3, 3, 4), dtype=int)
print(nda_all_zeros)

nda_all_ones = np.ones((3, 3, 4), dtype=int)
print(nda_all_ones)

# For creating random nd-array of integers. 
nda_all_random = np.random.random_integers(low=5, high=9, size=(3, 4, 4))
print(nda_all_random)

# While creating random nd-array of floats, you need to select one of the 
# distribution such as uniform, exponential, gamma etc and each distribution
# has it's own different parameters to consider. 
nda_all_random = np.random.uniform(low=5.0, high=9.0, size=(3, 4, 4))
print(nda_all_random)

nda = np.array(
    [
        [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]],
        [[13, 14, 15, 16], [17, 18, 19, 20], [21, 22, 23, 24]],
        [[25, 26, 27, 28], [29, 30, 31, 32], [33, 34, 35, 36]]
    ]    
)
print(nda)

# 2. Get the shape of the arreay.
print(nda)

# 3. Rshape an array.
reshape_nda = np.reshape(nda, (3, 12))
print(reshape_nda)

nda = np.reshape(reshape_nda, (3, 3, 4))
print(nda)

# 4. Accessing the array parts. 
# At this point human perceptions fail as human is designed to imagine in 
# 2D only. So all the study would be using 2D. And in practical implimentation
# it would be extended to more dimensions as per the requirement.

nda = np.array(
    [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12]
    ]   
)
print(nda)

elements = np.take(nda, [0, 1], axis=0)
print('**\n', elements)

