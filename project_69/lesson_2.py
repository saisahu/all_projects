# %%
"""This is to do practice with Pandas in Python.
"""
# While using pandas, there are specefic things that needed to be mastered.
# Those are
#   1. How to create a table (data frame) with all the required columns.
#   2. How to add data (the rows and columns) to it.
#   3. How to update data of the table (mutilple rows and columns).
#   4. How to How to get data of the table (multiple rows and colums).
#   5. How to create new table from the existing tables.
#   6. How to search through the rows and get the rows where a condition is 
#      satisfied.

import pandas as pd 

# 1. How to create a table. This requires setting the colums with their names. 
data_frame = pd.DataFrame(
    columns=['Sl_No', 'Name', 'Age', 'DOB', 'Blood_Group']
)
display(data_frame)

# 2. Adding data to the table. This means adding rows. You need to also assign
# the column names to the the new table that needs to be added to the 
# original one. For this we use 'concat' function of pandas and this takes the 
# tables to be added in a list. axis=0 for adding through x axis, means for 
# adding rows. Here any missing data is denoted as 'None'.
data_frame_to_add = pd.DataFrame(
    columns=['Sl_No', 'Name', 'Age', 'DOB', 'Blood_Group'],
    data=[
        ('1', 'Keith', '26', '1993-10-08', 'A+'),
        ('2', 'Roby', '21', '1998-01-12', 'B+'),
        ('3', 'Sai', '23', '1997-11-06', 'B+')
    ] 
)
display(data_frame_to_add)

data_frame = pd.concat([data_frame, data_frame_to_add], axis=0)
display(data_frame)

# 2. How to add extra columns to the original table. Here you need to mention
# axis=1 for adding through y axis, means adding colums. Any missing data will 
# be added as 'NaN'.
data_frame_to_add = pd.DataFrame(
    columns=['Profession'],
    data=[
        ('Seniour Software Developer'),
        ('Data Scientist'),
    ]
)
display(data_frame_to_add)

data_frame = pd.concat([data_frame, data_frame_to_add], axis=1)
display(data_frame)

# 3. How to update multiple rows and colums of the table.
display(data_frame.iloc[1:3, 1:4])
data_frame.iloc[1:3, 1:4] = [
    ('Joly', 27, '1993-07-11'),
    ('Goth', 21, '1999-03-09')
]
display(data_frame)

# 4. How to get data from the table. 
required_data_frame = data_frame.iloc[1:3, 1:4]
display(required_data_frame)

# 5. How to create new table from existing tables.
# You can use 'iloc' to get tables from different tables. You can concat those
# data frames to create new data frames both in a horizontal and vertical way. 
# No fixed way to do it. You need to take care of how exaclty you want to do 
# it.

# 6. How to search through the rows and get the rows where a specific condition
# is satisfied. 
# Find all the rows where column-Age = '26' and column-Name = 'Keith' and make 
# a new data frame with only taking those rows and all the columns. 
required_truth_series = (
    (data_frame['Age'] != '26')
    & (data_frame['Name'] != 'Keith')
)
required_data_frame = data_frame[required_truth_series]
display(required_data_frame)

# With these 6 skills of Pandas now you can do anything you want to do.


# %%
# To make an example that with the above skills you can solve anything that 
# that's related to pandas, here's an sample problem.
# Find all the row indeces where column-Name != 'Goth', and make a list of 
# those row indeces. 

required_truth_series = (
    (data_frame['Name'] != 'Goth')
)
required_data_frame = data_frame[required_truth_series]
display(required_data_frame)
print(required_data_frame['Sl_No'])
required_list_ = required_data_frame['Sl_No'].tolist()
print(required_list_)
required_list_ = [int(item) for item in required_list_]
print(required_list_)
print(type(required_list_[0]))


# %%
# Find the name of the columns/column where the 2nd row has a value of 
# '1993-07-11'
display(data_frame.iloc[1, :])
required_truth_series = (
    (data_frame.iloc[1, :] == '1993-07-11')
)
print(required_truth_series)

for index, item in enumerate(required_truth_series):
    if item == True:
        required_index = index
        break 
print('the required index is:', required_index) 
required_column_name = data_frame.columns[required_index]
print('So the required column name is:', required_column_name)

# Finding the time taken for the execution of the above code using time it 
# module.
stmt = '''display(data_frame.iloc[1, :])
required_truth_series = (
    (data_frame.iloc[1, :] == '1993-07-11')
)
print(required_truth_series)

for index, item in enumerate(required_truth_series):
    if item == True:
        required_index = index
        break 
print('the required index is:', required_index) 
required_column_name = data_frame.columns[required_index]
print('So the required column name is:', required_column_name)
'''

setup = '''import pandas as pd 
data_frame = pd.DataFrame(
    columns=['Sl_No', 'Name', 'Age', 'DOB', 'Blood_Group'],
    data=[
        ('1', 'Keith', '26', '1993-10-08', 'A+'),
        ('2', 'Roby', '21', '1993-07-11', 'B+'),
        ('3', 'Sai', '23', '1997-11-06', 'B+')
    ] 
)
'''

import timeit
time_taken = timeit.timeit(stmt=stmt, setup=setup, number=1)
print('Time taken for the above code to be executed is (in Sec.): \n' + 
    str(time_taken)) 