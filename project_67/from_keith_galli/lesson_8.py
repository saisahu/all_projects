# %%
"""This is lesson 8 and the video 8 of Keith Galli channel.

Here we will make a game using python and know how to actually make a good 
valuable project.
"""
import pygame 
import sys
import random
 
pygame.init()

# WIDTH = 900
# HEIGHT = 400

# RED = (190, 80, 5)
# BLUE = (12, 120, 190)
# YELLOW = (91, 170, 10)
# BACKGROUND_COLOR = (20, 20, 20)


player_size = 50
player_pos = [WIDTH/2, HEIGHT-2*player_size]

# enemy_size = 50
# enemy_pos = [random.randint(0,WIDTH-enemy_size), 0]
# enemy_list = [enemy_pos]

# SPEED = 5

MOVE_SIZE = 50

# screen = pygame.display.set_mode((WIDTH, HEIGHT))

# 1. Never pass directly any value to something if that value is key to make a 
# huge change in your code's output. First assign the values to some variable
# and the pass those variables to everywhere so that a single change in the 
# assignment line will make a change throughout the entire code.   

# game_over = False 

# score = 0

# clock = pygame.time.Clock()

# my_font = pygame.font.SysFont('monospace', 35)

# def set_level(score, SPEED):
#     if score < 20:
#         SPEED = 5
#     elif score < 40:
#         SPEED = 8
#     elif score < 60:
#         SPEED = 12
#     elif score > 60:
#         SPEED = 15
    
#     return SPEED

# def drop_enemies(enemy_list):
#     delay = random.random()
#     if len(enemy_list)<5 and delay<0.1:
#         x_pos = random.randint(0, WIDTH-enemy_size)
#         y_pos = 0
#         enemy_list.append([x_pos, y_pos])

# def draw_enemies(enemy_list):
#     for enemy_pos in enemy_list:
#         pygame.draw.rect(
#             screen, BLUE, (enemy_pos[0], enemy_pos[1], enemy_size, enemy_size)
#         )

# def update_enemy_positions(enemy_list, score):
#     for idx, enemy_pos in enumerate(enemy_list):
#         if enemy_pos[1]>=0 and enemy_pos[1]<HEIGHT:
#             enemy_pos[1] += SPEED
#         else:
#             enemy_list.pop(idx)
#             score += 1
#     return score

def collision_check(enemy_list, player_pos):
    for enemy_pos in enemy_list:
        if detect_collision(enemy_pos, player_pos) == True:
            return True 
    return False

# def detect_collision(player_pos, enemy_pos):
#     p_x = player_pos[0]
#     p_y = player_pos[1]

#     e_x = enemy_pos[0]
#     e_y = enemy_pos[1]

#     if e_y > (p_y-enemy_size) and ((e_x > (p_x-enemy_size)) and (e_x <  
#             (p_x+player_size))): 
#         return True
    
#     return False

while not game_over:
    
    for event in pygame.event.get():

        if event.type == pygame.QUIT:
            sys.exit()

        if event.type == pygame.KEYDOWN:

            x = player_pos[0]
            y = player_pos[1]

            if event.key == pygame.K_LEFT:
                x -= MOVE_SIZE
            elif event.key == pygame.K_RIGHT:
                x += MOVE_SIZE

            player_pos = [x, y]

    screen.fill(BACKGROUND_COLOR)

    drop_enemies(enemy_list)
    score = update_enemy_positions(enemy_list, score)

    SPEED = set_level(score, SPEED)
    
    # text = 'Score: ' + str(score)
    # label = my_font.render(text, 1, YELLOW)
    # screen.blit(label, (WIDTH-200, HEIGHT-40))

    if collision_check(enemy_list, player_pos):
        game_over = True

    # draw_enemies(enemy_list)

    # pygame.draw.rect(
    #     screen, RED, (player_pos[0], player_pos[1], player_size, player_size)
    # )

    # clock.tick(30)

    # pygame.display.update()

# 2. Also rather than defining directly using some numbers, always define the 
# variable and pass it in to where it is needed so that the name of the 
# variable would make sense where as the only raw declaration might not make 
# any sence. The raw numbers of the declarations are called magic numbers and 
# we need to assign these magic numbers to some variable then use those 
# variables.

# 2. Whenevere you stuck any where. The first option to try out is to try to 
# find a way to do that yourself (With this you will get a lot of flexibility 
# to modify your code as you have written the code). If not possible the second 
# option is to check on the official document website of the thing that you are 
# using (By doing this, you will have a good root level overview of what you 
# are doing and what more you can do with this library and how exactly things 
# are working in this library.). If it's taking too long to do get what you 
# want form the official documentation, then directly make a google search.(
# That's the worst way to do anything as it not increasing your base knowledge 
# of what you are doing) 

# 3. Whenever you wanna change a gobal variable from inside a fucntion, rather 
# than declaring it inside a function as a global, jsut pass it to the 
# function, midify it, then return it and finally assign that function to the 
# global variable itself to assign the new value to it. This is a professional 
# way to do it.

# 4. Whenever you need to get both the index and the value from a list, don't 
# get the value by it's index by using range of length of the list. Use 
# enumerate to get the index and the value simultaneously. This is a cooler and 
# intuative way to do this thing.


