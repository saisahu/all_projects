# %%
"""This is lesson 6 and the video 6 of Keith Galli channel.

Here it is explained about all you need to know about lists and tuples.
"""

# 1. List is a collection where you can store integers, strings, booleans.
# Tuples are just slightly different version of lists.

grocery_list = ['milk', 'avocados', 'hot pockets']

grocery_list.append('bread')
grocery_list.append('butter')

# 2. You can add anything at the end of the list.

grocery_list.pop()

# 3. You can remove the item at the end of the list.

grocery_list.pop(1)

# 4. You can remove any items by it's index from the list.

# 5. In python and many other programming language the indexing starts form 
# 0 rather than 1. So may be intuatively we may say milk is the first element
# (look at out example), but in python it is the 0th index and avocado is the
# first index and so on.

grocery_list.pop(-2)

# 6. You can also remove elements by negative indexing. 
#  0  1  2  3  4  5 (This is positive indexing for a list) 
# -6 -5 -4 -3 -2 -1 (This is negative indexing for a list)        

# 7. As 0 is already an idex for positive indexing so 0 can't be used for any 
# other kind of listing to avoid conflicts. In negative indexing it stats from
# -1.

# 8. When popping up by different indices always remember that each time you 
# pop up a value from a list the indexing of all the elements of the list 
# changes. Then the further pop should be based on new indexing of the 
# list elements.

# print(grocery_list)

# %%
grocery_list = [
    'rice', 'dal', 'bread', 'butter', 'milk', 'hot pockets',
    'avocados'
]

print(grocery_list[1])

# 9. You can get the element of the list by it's index.

print(grocery_list[2:5])

# 10. You can also get multiple adjacent values from the list. While doing this
# kind of indexing the first number at which the indexing starts, the number
# before the last number is where the indexing ends.

print(grocery_list[:5])

# 11. You can get the elements from 0 to the end of an index.

print(grocery_list[5:])

# 12. You can also get the elements from an index to the end of the list.

print('->', grocery_list[3:20])

# 13. While selecting a bunch of adjacent elements from a list using index 
# bounds, if you set the upper bound of the index to more than the maximum 
# index the list has, then it won't show you any error. Rather it will return 
# you from the starting bound of the index mentioned, to the end bound of the 
# index that the list has.

print(len(grocery_list))

# 13. You can get the length (number of elements) in a list.

grocery_list.insert(5, 'chikki')

print(grocery_list)

# 14. You can insert a new value to exactly at perticular index in the list.

grocery_list[5] = 'sauce'

grocery_list[2:5] = ['nuts', 'meyonnaise', 'soyabin']

# 15. You can also insert value to a list by assigning a value to a perticular
# index in a list. This is a cool way as you can insert multiple cosecutive 
# values simultaneously by assiging a list of values at a  lice of indices 
# of the list.

print(grocery_list.index('dal'))

# 16. You can get the index of a value from the list. Make sure to write the 
# spelling of the value correctly.

grocery_list.remove('dal')

print(grocery_list)

# 17. You can also remove an element by it's name from the list.

# %%
grades = [100, 12, 45, 60, 64, 83, 100, 100, 45]

grades.sort()

# 18. You can sort the list (small to big).

grades.reverse()

# 19. You can reverse the list. This is normally used in sorting the list from
# big to small by first sorting the list from small to big then applying 
# reverse.

print(grades)

print(grades.count(100))

# 20. You can count a perticular value in a list by it's name.

# %%

# 21. The basic difference in lists and tuples is tuples are unmutable.This 
# means they can't be dynamically changed once defined.

x, y, z = (1, 2, 3)

print(x, y, z)

def update(a, b, c):
    x = a + 5
    y = b - 1
    z = c + 3

    return (x, y, z)

print(update(1, 2, 3))

x, y, z = update(1, 2, 3)

print(x, y, z)

# 22. A nice thing about the tuples is that they can be unpacked.

# %%
name_list = ['keith', 'bob', 'smith']

print('keith' in name_list)

print('Sai' in name_list)

# 23. You can check whether a vlaue exists in a list or not by using 'in' key
# word.

