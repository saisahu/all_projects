# %%
"""This is lesson 3 and the video 3 of the Keith Galli channel.

Here it is explained everything one need to know about turtle graphics library
in Python.
"""
import turtle

# bob = turtle.Turtle() 

# bob.forward(100)
# bob.left(45)
# bob.forward(100)
# bob.right(90)
# bob.forward(100)

# turtle.done()

# 1. You can basically make a window open with a bob on it with turtle.
# Then you can move the bob in different ways. Forward, backward
# and change the angle by some degrees left or right.

# %%
# bob = turtle.Turtle()

# bob.color('green', '#fca903')
# bob.begin_fill()
# bob.forward(100)
# bob.right(90)
# bob.forward(100)
# bob.right(90)
# bob.forward(100)
# bob.right(90)
# bob.forward(100)
# bob.end_fill()

# # 2. You can choose the color of the line and also choose the color to fill it.

# bob.penup()
# bob.forward(100)
# bob.pendown()

# # 3. You can also pen up for move the bob without drawing anything and again
# # pen down to activate the draw mode again.

# bob.color('green', '#fca903')
# bob.begin_fill()
# bob.forward(100)
# bob.right(90)
# bob.forward(100)
# bob.right(90)
# bob.forward(100)
# bob.right(90)
# bob.forward(100)
# bob.end_fill()

# turtle.done()

# %%
# bob = turtle.Turtle()

# bob.speed(1)

# # 4. You can change the speed of the bob.

# bob.color('red', 'yellow')
# bob.begin_fill()
# for i in range(30):
#     bob.forward(200)
#     bob.right(165)
# bob.end_fill()

# 5. You can use a for loop to again and again do the same thing in turtle so 
# to create some cool shapes. If the looping is set too high the bob will
# just move again the trajectory without affecting anything.

# turtle.done()

# %%
# import math

# bob = turtle.Turtle()

# bob.speed(10)

# for i in range(100):
#     bob.forward(100)
#     bob.right(math.sin(i%90))

# turtle.done()

# 6. You can play with any maths to draw different cool designs. 

# 7. The more your programming skill is the more complex design you can make 
# with turtle. That's the scalel of your expertisation.

# 8. If you are good in turtle drawings, then you are good in geometry,
# algebra, loopings and conditonals simultaniously. That means you are a full
# stack programmer.
