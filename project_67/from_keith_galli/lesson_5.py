# %%
"""This is lesson 5 and the video 5 of Keith Galli channel.

Here it is explained all you need to know about functions.
"""
# 1. functions break your codes to small reusable components and make your 
# code simple.

# 2. with functions you don't need to write the same code againg and again.
# Only you need to place the code in a function suit and call that function
# agian and again just only writting only one line of code.

# 3. Also you can add dynamic nature to the function by passing variables to 
# it and using them and return or do something based on it. Whenever you call
# this kind of functions you need to always pass a set of variables that this 
# function requires to use them for doing anything and returning it.

# 4. In order to get an output from a function just print the output from 
# inside the function or return the output value. So that when the function is 
# assigned to a variable, actually the value returned from the function will be
# assinged the variable. This strategy allows us to capture the output value of 
# a function. 

