# %%
"""This is lesson 4 and the video 4 of Keith Galli channel.

Here it is explained about all you need to know about python conditionals.
"""

grade = 82

if grade < 60:
    print('You are failed!')
elif grade <= 70:
    print('You are passed! You get a C grade! Nice!')
elif grade <= 80:
    print('You are passed! You get a B grade! Super!')
else:
    print('You are passed! You get a A grade! Amazing!')


# 1. the suit inside an if statement is executed if the condition is true. Is 
# the condition is false then the pointer jumps to the next elif statement and
# so on and finally it jumps upto the else statement.

# %%
a = 2; b = 3
print(a == 8)
print(a == 2)
print(a==2 and b==3)
print(a==3 and b==7 or 1<-1)

# 2. You can print the truth values of different statements using a print 
# statement.

print(a<5 and (b>7 or 1!=8))

# 3. You can use brackets to the combination of different combinations of 
# statements to build a combined condition. 

# 4. It is not necessary that you should use an else statement at the end.

# 5. An if and elif statement always need a condition, where as an else 
# statement doesn't take any conditon with it and it always needs to be 
# followed after if or elif statements.

# 6. <, >, <=, >=, ==, !=, and, or, not, () are called the operators.

# 7. Always try to write different if statements in one line of code and with
# a long line of code rather than nesting them as nesting the statements makes
# the logic hard to uderstand.
 