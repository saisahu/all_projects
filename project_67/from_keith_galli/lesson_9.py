# %%
"""This is lesson 9 and the videos 9 of Keith Galli channel.

Here it is explained all you need to know about classes in python.   
"""
# 1. Classes made our code bug free as it is a cleaner way to code.

# 2. Classes made our code easier to read by providing structures to the code.

# 3. You can easily add more functionality by using classes as you can add more
# methods to the class and also can crate more classes. 

# 4. You can use testing to a perticular class withour worring about other 
# parts of your code. 

import turtle

class Polygon:
    def __init__(self, sides, name, size=100, color='black', line_thickness=2):
        self.sides = sides
        self.name = name
        self.size = size
        self.color = color
        self.line_thickness = line_thickness
        self.interior_angle = (self.sides-2) * 180
        self.angle = self.interior_angle / sides

    def draw(self):
        turtle.color(self.color)
        turtle.pensize(self.line_thickness)
        for i in range(self.sides):
            turtle.forward(self.size)
            turtle.right(180-self.angle)
        # turtle.done()

# 5. To define a class you just need to initiate with a class keyword and add
# an init method to it with some important variable assignment strategies. 
# That's all. You have defined a class.

square = Polygon(4, 'Square')
pentagon = Polygon(5, 'Pentagon')

print(square.sides)
print(square.name)
print(square.interior_angle)
print(square.angle)

print(pentagon.sides)
print(pentagon.name)

# 6. When we call a class, we actually encapsulate the information to a 
# variable through that class. To use this kind of encapsulation strategy we 
# add an init method to the class.

# hexagon = Polygon(6, 'Hexagon', color='blue', line_thickness=10)
# hexagon.draw()

# 7. In order to do something based on the encapsulatd information to a 
# variable through a class, we also need to add more methods to the class those
# would use the encapsulated information and do something based on it. To 
# access the encapsulated information from a variable, we pass the self 
# variable to the class methods, through which we can access the encapsulated 
# information. 

# 8. If you don't define a default argument while setting the arguments in a 
# class or function, you must need to pass that argument while calling it. To 
# avoid each time passing an argument we assign a default argument value to 
# that argument while setting it.

# 9. You can also use a function as an encapsulated set instructions as a 
# reusable component, but functions can only hold instructions but not 
# variables, so each time you call it you also need to pass the variables as 
# arguments where as the class can hold both variables and instructions 
# simulteniously that makes them better choice to use as reusable components.
# Once you declared an object through a class and set it to a variable, now 
# you have the ability to just call the it's method without worring about 
# passing the variables again and agian.

# %%
class Square(Polygon):
    def __init__(self, size=100, color='black', line_thickness=2):
        super().__init__(4, 'Square', size, color, line_thickness)
    
    def draw(self):
        turtle.begin_fill()
        super().draw()
        turtle.end_fill()

square = Square(size=50, color='#abc123')
# square.draw()

# turtle.done()

# 10. You can redefine a new class that should contain all the variable 
# encapsulation strategies and all the methods of another class by using a 
# subclass of the first class. In the initialization of the subclass (the init
# method) you should only initiate the variable asigning strategies that really
# needs to be assigned to the class while instatiating it. All other variables
# of the first class can be added to the new subclass by calling the super 
# method inside it's init method. By defining the subclass you don't need to 
# write all the codes of variable assigning strategies and all methods again 
# inside the subclass. That is the advantage of the subclass.

# 11. You can also override any of the method of the previous class in the
# subclass.


# %%
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, other):
        if isinstance(other, Point):
            x = self.x + other.x
            y = self.y + other.y
            return Point(x, y)
        else:
            x = self.x + other 
            y = self.y + other
            return Point(x, y)
    
point_1 = Point(1, 2)
point_2 = Point(4, 7)

point_3 = point_1 + point_2 

print(point_3.x, point_3.y)

point_4 = point_1 + 5

print(point_4.x, point_4.y)

# 12. You can use any operator between two encapsulations in a way you want.
# +, -, >= etc. are operators. Setting a strategy of how the object 
# should react to different operator is called operator overloading. You can't 
# override any logical operator such as not, or, and etc.

# %%

