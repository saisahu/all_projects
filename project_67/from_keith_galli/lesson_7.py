# %%
"""This is lesson 7 and the video 7 of Keith Galli channel.

Here it is explaned all about you need to know about looping in python 
programming.
"""

# 1. The reason loops are used in python programming is to repeatedly execute 
# blok of codes.

for i in range(10):
    print(i)

# 2. The i is just like a placeholder for us and it could be anything. It just 
# holding the value of the iteration for us. 

# 3. While using range for for loops, always remember that it starts from 0 and 
# goes all the way up to 9. last value is excluded. But as it starts from 0 and 
# not from 1 it actually doing the no of iterations equals to the number 
# mentioned as the range parameter.

# %%
grocery_list = ['milk', 'avocado', 'bananas', 'toast']

for items in grocery_list:
    print(items)

# 4. You can go through the every element of a list using the for loop.

# 5. Once you can go through all the elements of a list, you can even do a lot
# of different things with each iteration depeanding on how you have written 
# the suit inside the for loop.

for i in range(len(grocery_list)):
    print(grocery_list[i])

# 6. You can also get the indices of a list and go through it using a for loops 
# to and can access the elements by their indexes or you also can go up or down
# with the index value and in a perticular iteration to access other elements 
# of a list. Noramlly the placeholder in this case it taken as i stnds for 
# index.

# 7. for loops are used for running over the collections.

# 8. While loops are actully an if statement that repeatedly checks itself 
# over and over again before executing the codes inside it's suit.

while True:
    print('Hello world!')

# 9. A while statement just keep running until so far the condition attached is 
# true. The moment it gets a false statement it immedeately stop executing the 
# suit inside it and stops for ever.

# 10. while loops are basically used to control the flow based on a certain 
# condition. If you alrady know how many times you need to repeat the loop
# then for loop is a nice shortcut and it's a cool way to code that thing. 
# That's the only reasons to use for loops.

# %%
for i in range(10):
    print(i)
    if i > 5:
        break
# 11. You can immediately break a loop based on some certain condition. The 
# break statement exit out of the loop. Then it takes the pointer down.

# %%
for i in range(10):
    if i == 5:
        continue
    print(i)

# 12. You can skip a certain amount of code inside the the suit in a loop based
# on a certain condition and go for the next executions. The continue statement
# stops the loop immediately where it is encountered and then it takes up the 
# pointer up onto the loop. 

    
    



