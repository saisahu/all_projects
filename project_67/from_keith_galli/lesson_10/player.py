from color import Color
import pygame

class Player:
    def __init__(self, x, y, size, color=Color.RED):
        self.x = x
        self.y = y 
        self.size = size 
        self.color = color 
        
    def draw(self, screen):
        pygame.draw.rect(
            screen, self.color, (self.x, self.y, self.size, self.size)
        )
    
    def detect_collision(self, other):
        if self.y > (other.y-self.size) and ((other.x > (self.x-other.size)) 
                and (other.x < (self.x+self.size))):
            return True 
        return False

class Enemy(Player):
    def __init__(self, x, y):
        super().__init__(x, y, size=50, color=Color.BLUE)

class LargeEnemy(Player):
    def __init__(self, x, y):
        super().__init__(x, y, size=100, color=Color.BLUE)

class KeithEnemy(Player):
    img = pygame.image.load('../assets/didi.png')
    def __init__(self, x, y):
        super().__init__(x, y, size=100, color=Color.BLUE)

    def draw(self, screen):
        rect = pygame.Rect(self.x, self.y, self.size, self.size)
        scaled_img = pygame.transform.scale(self.img, rect.size)
        scaled_img = scaled_img.convert()
        screen.blit(scaled_img, rect)

class HumanPlayer(Player):
    def __init__(self, x, y):
        super().__init__(x, y, size=50, color=Color.RED)