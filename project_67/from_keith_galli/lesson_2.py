# %%
"""This is the lesson 2 and the video 2 of the Keith Galli channel.

Here it is explained the basics of Python.
"""

import sys 
print(sys.version)
# 1. You can get which version of python you are using using sys package and 
# version module.

print('Sai is a good boy!')
# 2. You can print anything in Python using print function.

# %%
print(2+3) # + for addition
print(2*3) # * for multiplcation
print(2**3) # ** for exponent
print(2-3) # - for substraction
print(6/2) # / for division
print(10//3) # // integer divisio
print(10%3) # % for remainder (modular devision)

print(((2+2)*5) - (8//3)) # complex calculations

# 3. You can do any kind of arithmatic calculations in Python this way.
# To get the result just print it out.

import math 
print(math.sin(math.pi / 2))

# 4. You can do more complex math operation using math library. There are a lot 
# more libraries availabel out there in python for math. For more ideas 
# regarding a perticular library you can go for it's documentation.

# %%
age = 23 # an interger nummber
age_2 = 23.6 # a decimal number
name = 'Sai' # a text
is_python_fun = True # a boolean
is_java_fun = False # a boolean

print(age, age_2, name, is_python_fun, is_java_fun)
print('age, age_2, name, is_python_fun, is_java_fun')

# 5. You can store any value to a variable name. This includes number(integers, 
# decimals), text and boolean.

# 6. You can print the variable by it's name. The reason we use variable in 
# programming is to get a sense of what this value is. For example age, name,
# is_python_fun etc. The name of the variable should be descriptive and not too
# long.

# 7. You can use letters(both uppercase and lowercase), underscores and 
# numbers. A variable name could not be started with a number.

# %%
a = 3
b = 5
c = a + b
print(c)

# 8. You can also set variables that have combiniton of other variables.

a = 7
print(c, a)

# 9. If you again declare a new variable, then the previous variable that holds
# the value of the combinition of the older variable never changes (look at the
# example above). 





