"""This is the lesson 1 and the video 1 of the Keith Galli channel. 

Here it is explained that how to a basic python setup.

Setting up Python
-----------------
1. Download the latest stable version of python. As per now python 3.8 is the 
most stable one. Don't go with the latest version. You may face a lot of 
issues.

2. Install virtualenv using pip. Then create a virtual environment (tf). Now 
update everything inside that virtual environment.

3. Then install the following package inside the virtual environment.
    - numpy
    - pandas
    - matplotlib
    - scipy
    - sklearn
    - tensorflow
    - notebook ( for Jupyter Notebook)

Setting up the Code Editor
--------------------------
1. Now install the latest version of VS Code.  

2. Turn on auto save option. Set the delay for auto save to 200 milli seconds.

3. Then add a ruler at the 79th characher at the right side of the editor to 
never cross the 79 characters in one line.

4. Select the python interpreter (python.exe) from the virtual environment.

5. The code completion feature is inbuilt in VS Code.

6. Install pylint for linting while coding in VS Code in your virtual 
environment.

7. Setup git and create some projects (folders) in it. Add a project folder 
to VS Code by dragging it onto VS Code.

You are ready to get started with the python!
"""