# %%
"""This is lesson 1 and my 1st self study of python programming. 

Here it is explained all about all the inbuilt function of Python.
There are 67 python inbuilt functtions that needed to be memorized. 
"""
print(abs(-45.5)) 
# 1. abs() - Returns the absolute value of a function.

print(all([True, 4<5, 5+1 == 3+3])) 
# 2. all() - Returns True if all the members of list/tuple are true.

print(any([False, True]))
# 3. any() - Returns True if any of the members of an iterable object is True. 

print(ascii('This is an Ω omega sign. Eita କ ate.'))
print('\u03a9, \u0b15')
# 4. ascii() - Returns the equivalent escape characters of any non ascii 
# character. (ascii characters are those characters that are available on 
# key board and other characters such as pi, omega, odiya, hindi characters
# are non ascii characters)

print(bin(5)) 
# 5. bin() - Returns the binary number of any number. (decimal, octal, 
# hexadecimal)

print(bool(4 < 5 and 3 == 1+2))
# 6. bool() - Returns the truth valur of any expression.

print(bytearray(2))
# 7. bytearray() - Returns number of bytes as an array called bytearray.

print(bytes(2))
# 8. byte() - Returns byte for a any numer.

number = 5
def print_something():
    print('x')
class Pen():
    def class_method():
        pass
print(callable(print_something))
print(callable(Pen))
print(callable(Pen.class_method))
print(callable(number))
# 9. callable() - Returns True if variable is callabe. Actually in the term 
# def my_function, my_function is a variabel that is callabe. Similarly in 
# class Pen, Pen is callabe. Pen.class_method is also callabe. 

print(chr(80))
# 10. chr() - Returns the character that a unicode represent. (Each character 
# has a unicode that represents that character)

# classmethod()
# 11. !! classmethod() not sure about this.

# text_code = '''print('x')
# for i in range(3):
#     print(i)

# def a_function():
#     return 'y'

# print(a_function())
# '''
# code = compile(text_code,'nope', 'exec')
# exec(text_code)
# 12. compile() - Returns a code object that is ready to be executed. You need 
# to further excecute the object returned by a compile method using exec() inbuilt 
# function.

print(complex(5, 7))
# 13. complex() - Returns a complex number object. 

class Pen:
    thing_name = 'Pen'

    def __init__(self, color, brand):
        self.color = color
        self.brand = brand
    
    def write(self):
        print('This is a {} of {} in' 
        ' {} color.'.format(self.thing_name, self.brand, self.color))

delattr(Pen, 'write')
 
pen = Pen('Blue', 'Butter Flow')
# pen.write()
# 14. delaattr() - Delete any attribute of a class. Attribute means class 
# constant variables, class variable variables, class methods etc. Basically it 
# means properties and methods.

print(dict(name='Sai', age='23', blood_group='B+')) 
# 15. dict() - Returns a dictionary object

class Apple:
    def __init__(self, color, type):
        self.color = color
        self.type = type 

    def show_yourself(self):
        print('I am an apple. My color is {}. I am'
         'a {}'.format(self.color, self.type))
        
apple = Apple('Red', 'Fruit')
print(dir(apple))
# 16. dir() - Returns all the names of attributes (all the properties and 
# methods) of an object.

print(divmod(6, 4))
# 17. divmod() - Returns the quotient and remainder when argument 1 is devided by 
# argument 2. 

enumerate_object = enumerate(['apple', 'ball', 'dog'])
for index, value in enumerate_object:
    print(index, value)
# 18. enumerate() - Returns an enumerate object that means that object holds
# both the index and the values of a sequential object that can be loop over 
# using a for loop. 

code = '5 > 6'
eval(code)
# 19. eval() - Execute a single lined code that is in text form.

multilined_code = '''print('The grass is green.')
print(9)
'''
exec(multilined_code)
# 20. exec() - Executes multilined codes that is in text form.

def filter_function(x):
    if x > 3:
        return x
    else: 
        pass

filtered_list = filter(filter_function, [1, 2, 3, 4, 5, 6])
for x in filtered_list:
    print(x)
# 21. filter() - Takes each element of a sequence at a time, pass it through 
# the function, for each element generates an output through the function and 
# store all the values in a sequential object and returns it. 

print(float('30'))
# 22. float() - Returns a floating point number object from an integer object 
# text object.

print(format(0.6, '%'))
# 23. format() - Returns a new format of a value as specified in the 2nd 
# argument.

print(frozenset([1, 2, 3, 3, 4, 4]))
# 24. frozenset() - Returns a frozen set form a list means it returns a set 
# that could not be changed.

class Pen:
    category = 'Study'

    def __init__(self, color, brand):
        self.color = color 
        self.brand = brand 

    def write(self):
        print('This is a pen and it can write.')

pen = Pen('Blue', 'Cello')

print(getattr(pen, 'category'))
# 25. getattr() - Return the value of a specific attribute of an object. 

# print(globals())
# 26. globals() - Returns a dictionary of all global variables (all the global
# objects). This function does not work with interactive python but works fine
# while run using a cmd. 

print(hasattr(pen, 'write'))
# 27. hasattr() - Returns true if a specific object has a specific attribute. 

print(hash(pen))
# 28. hash() - Returns a hash value of a specific object. If two objects are same
# the hash value of the two objects also needed to be same. It is used in quick
# look up for finding seimilar elements in a list. I don't know how exactly to 
# use it.

# help()
# 29. help() - Executes the Python's inbuilt help system. 

print(hex(7))
# 30. hex() - Returns the hexadecimal value of any number. 

print(id(pen))
# 31. id() - Returns the id of an object. 

# print(input())
# 32. input() - Allows user input.

print(int('57'))
# 33. int() - Returns integer of any text number, float etc. 

print(isinstance(pen, Pen))
# 34. isinstance() - Returns True if a specific object is an instance of a 
# specific Class. 

class Pencil(Pen):
    pass

print(issubclass(Pencil, Pen))
# 35. issubclass() - Returns True if a specific class is a subclass of a 
# specific class.

list_ = ['apple', 'banan', 'guava']
list_iterator_object = iter(list_)
print(next(list_iterator_object))
print(next(list_iterator_object))
print(next(list_iterator_object))
# 36. iter() - Returns itarator object of a sequential object
# (list, tuples, sets, not supported for dictionaries).

print(len([1, 2, 3, 5, 8, 3]))
# 37. len() - Returns the lenght of a sequential object including texts.

print(list((1, 2, 'apple', 'banana', 7)))
# 38. list() - Retrns a list object 

# print(locals())
# 39. locals() - Returns a dictionary of all local variables in a perticular 
# scope (For Example inside a funciton). 

def the_map(x):
    return x+2
maped_values = map(the_map, [1, 2, 3])
for x in maped_values:
    print(x)
# 40. map() - Takes each value from an sequential object, pass it to a 
# function, and return each value as in a itarator. You can loop over it to 
# find all the values. 

print(max([1, 2, 3, 5, 9, 4]))
# 41. max() - Returns the maximum value of an sequential object.

print(memoryview(b'apple'))
# 42. memoryview() - Returns the memoryview object on a bytelike object. I 
# really don't know it's usage or why do even we need it. 

print(min([1, 2, 3, 4, 9, 0]))
# 43. min() - Returns the smallest item of an seqential object. 

itarable = iter([1, 2, 3, 4])
print(next(itarable))
print(next(itarable))
# 44. next() - Returns the next value of an iterable object that is made from 
# an iterator. 

print(object())
# 45. object() - Returns the a new object with all the default propertied and 
# methods a Python object has as inbuilt. 

print(oct(5))
# 46. oct() - Return the number in ocat system. 

file_ = open('demofile.txt', 'r')
print(file_.read())
# 47. open() - Opens a file and returns a file object. 

print(ord('c'))
# 48. ord() - Returns the integer that is the unicode of the letter.

print(pow(2, 3))
# 49. pow() - Returns the pow of the 1st argument to the 2nd argument. 

print('x')
# 50. print() - Prints anything on the consol.

# property()
# 51. property() - !! I don't know what it does. Needed to be looke again.

range_ = range(1, 4)
print(range_)
for number in range_:
    print(number)
# 52. range() - Returns a sequential set of numbers within a specified 
# interval.

print(repr(pen))
# 53. repr() - Returns a readable version of an object. 

reverse_ = reversed([1, 2, 3, 4, 5])
for number in reverse_:
    print(number)
# 54. reverse() - Returns a riversed sequential object of another sequential 
# object. 

print(round(56.9))
# 55. round() - Returns a rounded number means the smaller integer value neared to 
# the float. 

print(set((1, 2, 3, 'apple')))
# 56. set() - Returns a set object. 

setattr(Pen, 'a_new_property', 'Pen is a study material.')
print(pen.a_new_property)
# 57. setattr() - Set a variable to a class. Only applicable for class 
# variable and does not work for instance variables. 

slice_ = slice(2, 8, 2)
print(slice_)
print(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'][slice_])
# 58. slice() - Returns a slive object that could be used to slice up a 
# sequential object. 

print(sorted([1, 5, 7, 3]))
# 59. sorted() - Returns a soreted list of any sequential object. 

# @classmethod()
# 60. @classmethod() - !! Really don't know what it does. So needed to go 
# through it. 

print(str(53.7))
# 61. str() - Returns a text object of any other object.

print(sum([1, 2, 3, 4]))
# 62. sum() - Returns sum of a sequential object. 

class Pencil(Pen):
    def __init__(self, color, brand, softness):
        # Instintiating the parent class variable institiation
        super().__init__(color, brand)

        # Institiating the own class variable institiation
        self.softness = softness

pencil = Pencil('Black', 'Cello', 'High')
print(pencil.color, pencil.brand, pencil.softness)
pencil.write()
# 63. super() - Instantiate the assigment of parent class variables. 

print(tuple((1, 2, 3)))
# 64. tuple() - Returns a tuple object. 

print(type(pencil))
# 65. type() - Returns the type of the object (that means which class it 
# belongs to)

print(vars(pencil))
# 66. vars() - Returns all the property name and value of an object in 
# dictionary format.

zip_ = zip([1, 2, 3], [4, 5, 6], [2, 3])
for item in zip_:
    print(item)
# 67. zip() - Returns a sequential object containing the respective indices of 
# of multiple sequential objects passed as arguments.

























