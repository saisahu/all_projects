# %%
"""
This is lesson 2 and my 2nd self study of python programming.

Here it is explained all about all the inbuilt data types (object types) and 
their properties and methods namely list, tuple, set, dictionary, string.
"""
# 1.  In this study we only need to know about different methods of the 
# sequential data types.This is because the inbuild data types don't really 
# have any properties associated with them. And there are really no methods 
# associated with the singular data tyepes such as int, float, boolean etc.

# 2. List has the most functionality in all the sequential data types. So 
# we will start from list and then go to tuple set.

# %%
# All list methods 
list_ = [1, 2, 3, 4, 5]

list_.append(6)
print(list_)
# 1. .append() - Adds an element to the end of the list. 

# list_.clear()
# print(list_)
# 2. .clear() - Removes all the elements from a the list.

list_copy = list_.copy()
print(list_copy)
# 3. .copy() - Returns a the same list. 

print(list_.count(3))
# 4. .count() - Returns the number of elements of perticular value  present in
# the list.

list_.extend([7, 8, 9])
print(list_)
# 5. .extend() - Adds a new set of elemets at the end of the list. Also useful 
# in adding multiple elements at a perticular index. 

print(list_.index(5))
# 6. .index() - Returns the idex at where it first encounters the value. 

list_.insert(2, 'apple') 
print(list_)
# 7. .insert() - Inserts a value exactly at a perticular index

list_.pop(2)
print(list_)
# 8. .pop() - Removes the value at a perticular index. 

list_.remove(5)
print(list_)
# 9. .remove() - Removes specific value that encountered first.

list_.reverse()
print(list_)
# 10. .reverse() - Reverse the order of the list.

list_.sort()
print(list_)
# 11. .sort() - Sorts the list. Works both for alphanumeric values at the 
# same time.

# All tuple methods
# Tuples are immutabel. So only count and index method is avilabe here. 
# The reason to use tuples is that those are 5 times faster than lists.

tuple_ = (1, 2, 3, 4, 5)

print(tuple_.count(3))
# 1. .count() - Returns the number of times a value is inside the tuple.

print(tuple_.index(3))
# 2. .index() - Returns the the index of the value where it first encounted in 
# the entire list. 

# All the set methods
# The reason why to use a set is not clear so far. May in future I will have a
# better understanding of this thing.

set_ = {1, 2, 3, 4, 5}
print(set_)

set_.add(6)
print(set_)
# 1. .add() - Adds an element to the set.

# set_.clear()
# print(set_)
# 2. .clear() - Remove all the elements of the set.

set_copy = set_.copy()
print(set_copy)
# 3. .copy() - Returns a copy of the set. 

print(set_.difference({5, 6, 7, 8, 9}))
# 4. .difference() - Returns the difference between two sets. 1st argument - 
# 2nd arguments.

set_.difference_update({5, 6, 7, 8, 9})
print(set_)

set_.discard(4)
print(set_)
# 5. .discard() - Removes a specific element from the set.

print(set_.intersection({2, 3, 4, 5, 6}))
# 6. .intersection() - Returns the intersection of the set with another set.

set_.intersection_update({2, 3, 4, 5})
print(set_)
# 7. .intersection() - Removes the element of the set that are not present in 
# the another set.

print(set_.isdisjoint({4, 5, 6}))
# 8. .isdisjoint() - Returns True if the set is disjoint with another set. 

print(set_.issubset({1, 2, 3, 4, 5}))
# 9. .issubset() - Returns True if the set is a subset of another set.

print(set_.issuperset({2}))
# 10. .issuperset() - Returns True if the set is a superset of another set.

set_.pop()
print(set_)
# 11. .pop() - Removes a value from the set. It removes an arbitary value from 
# the set.

set_ = {1, 2, 3, 4, 5} 

set_.remove(4)
print(set_)
# 12. .remove() - Removes the specified element of the set. 

print(set_.symmetric_difference({3, 5, 6, 7}))
# 13. .symmetric_difference() - Returns a set of symmetric difference between 
# the set and the another set. 

set_.symmetric_difference_update({3, 5, 6, 7})
print(set_)
# 14. .symmetric_difference_update() - Update the set with symmetric difference
# with another set. 

print(set_.union({1, 2, 8, 9}))
# 15. .union() - Returns a set that is a union of the set with another set.

set_.update({1, 2, 8, 9})
print(set_)
# 16. .update() - Update the set with the union of the set with another set. 

# All dictionary methods.

dict_ = {'name':'Sai', 'age':22, 'nationality':'Indian'}
print(dict_)

# dict_.clear()
# print(dict_)
# 1. .clear() - Removes all the elements of the dictionary.

dict_copy = dict_.copy()
print(dict_copy)
# 2. .copy() - Returns a copy of the dictionary.

dict_2 = dict.fromkeys(('place', 'city', 'country'), 'All Same')
print(dict_2)
# 3. .dict_2() - Returns a dictionary with a set of key passed to it as a 
# tuple, and all the key has the same value as passed to it as the second
# argument. 

print(dict_.get('name'))
# 4. .get() - Returns the value of a specified key from a dictionary.

print(dict_.items())
# 5. .items() - Returns a list containg tuples those contains all the key value
# pairs form the dictionary. 

print(dict_.keys())
# 6. .key() - Returns a list containing all the keys of the dictionary. 

dict_.pop('name')
print(dict_)
# 7. .pop() - Removes a key value pair form the dictionary with a specified
# key.

dict_.popitem()
print(dict_)
# 8. .popitem() - Removes the last inserted key-value pair. 

dict_.setdefault('last_name', 'Sahu')
print(dict_)
# 9. .setdefault() - Returns the value of a specified key. If the key does not 
# exists then this method inserts the key value pair that are passed to it as
# arguments. If in case the key and value you are searching for might not be
# included in the dictionary, you can use this method to set it without any 
# error and get a value of the key simultaniously. 

dict_.update({'Religion':'Hindu', 'God':'Vagwan'})
print(dict_)
# 10. .update() - Adds additional key-value pairs to the dicionary. The 
# arguments are needed to be passed also as dictionary. 

print(dict_.values())
# 11. .values() - Returns list of all values from the dictionary. 

# All string methods
# String is a very frequently used data type (object). So it is very important 
# to know all the methods of the string.  

string_ = 'this is a string. this is the next sentence.'
print(string_)

print('this is a string'.capitalize())
# 1. .capitalize() - Returns a string with the first letter of the string 
# capitalized.

print('ThiS is a string'.casefold())
# 2. .casefold() - Returns a string with all the letters in smaller case. 

print('apple'.center(20))
# 3. .center() - Returns a string center with a certain number of characters.

print('apple is a good fruit. So I love apple'.count('apple'))
# 4. .apple() - Returns a the number of time a specific text occures in the
# entire string. 

print('The name is Ståle'.encode())
# 5. .encode() - Returns the string with all the escape characters in escape
# format.

print('This is an apple'.endswith('apple'))
# 6. .endwith() - Returns True if a string ends with a specific text value.

print('This\tis\tan\tapple'.expandtabs(4))
# 7. .expandtabs() - Returns a string that has an expanded tab size as per the 
# argument. tab means a spece i.e \t in a string.

print('This is an apple'.find('is'))
# 8. .find() - Returns the index where a specific text value is found first.

print('This is a {} and this is an {}'.format('pen', 'apple'))
# 9. .format() - Fills the placeholders of a string by given aguments. Also 
# you can do :something inside that placeholder so as to show the result in 
# different way. 

print('This is a {object} and this is an {fruit}'.format_map(
    {'object':'pen', 'fruit':'apple'})
)
# 10. .format_map() - Fills the placeholder of a string with a value that is 
# associated with a key provided by a dictionary argyment. 

print('This is an apple.'.index('is'))
# 11. .index() - Returns the index at which a specified value is found. 

print('Thisisanapple'.isalnum())
# 12. .isalnum() - Returns True if all the elements of the string are 
# alpha-numeric. If the string contains any whitespaces, new line, special 
# characters or non ascii characters then this  value is false. 

print('Thisisanapple.'.isalpha())
# 13. .isalpha() - Returns true if all the characters in the string are only 
# alphabets.

print('35'.isdecimal())
# 14. .isdecimal() - Returns True if the string only contains decimal number.
# Here in this case, the method only returns True for non negative integers.

print('55'.isdigit())
# 15. isdigit() - Returns True if the string only contans a digit. Here in this
# case only non negative integers. 

print('the_first_copy_no_01'.isidentifier())
# 16. .isidentifier() - Returns True if the string is an identifier means the 
# string should only contain alphanumeric characters and underscores and must 
# not start with a number.

print('abcde01 5!'.islower())
# 17. .islower() - Returns True if all the alphabets in a string are small. 

print('1234'.isnumeric())
# 18. .isnumeric() - Returns True is all the characters in a string are 
# numeric.

print('apple_123!'.isprintable())
# 19. .isprintable() - Returns True if all the characters in a string are 
# printable.

print('        '.isspace())
# 29. .isspace() - Returns True if all the characters in a string are white 
# spaces.

print('The Art Of Tomorrow'.istitle())
# 30. .istitle() - Returns True if all the words in a string start with a 
# capital letter the other letters in those words are small.

print('THE ART 25_!'.isupper())
# 31. .isupper() - Returns True if all the letters of the strings are upper
# case latters. 

print('$'.join(['a', 'b', 'c'])) 
# 32. .join() - Returns a joining of all the element of the sequential data 
# that is passed as the argument with the string at the middle of each 
# two elements. 

print('apple'.ljust(20))
# 33. Returns a left justified version of the string within a given no. of 
# spaces that is passed as argument. 

print('Harman is 29.'.lower())
# 34. .lower() - Returns a lower case version of the string. 

print('This', '       apple       '.lstrip(), 'is good.' )  
# 35. .lstrip() - Returns a left strip version of the string means any white
# speces at the left side of the string is eliminated.

translation_table = str.maketrans('a', 'b', 'o')
print('apple is a good fruit.'.translate(translation_table))
# 36. .maketrans() - Returns a translation table that can further be used to 
# make a translated string that means replaces certain alphabets with oters 
# and remove certain alphavets form the main string.

print('This is an apple.'.partition('an'))
# 37. .partition() - Returns a tuple that has three elements. The middel item
# of that tuple is the argument passed to the method. The first and last items
# are the left characters and the right characters around the argumet's word 
# that is passed. 

print('This is an apple'.replace('an', 'red'))
# 38. .replace() - Returns a string with a specific word replaced with another
# word.

print('This is an apple is'.rfind('is'))
# 39. .rfind() - Returns the index where a specific word is found towards the 
# right most position.

print('This is an apple is'.rindex('is'))
# 40. .rindex() - Returns the index where a specific word is found towards the 
# right most position.

print('apple'.rjust(20))
# 41. .rjust() Returns a right justified version of the string inside the spaces provides as
# the argument. 

print('this is an apple'.rpartition('is'))
# 42. .rpartition() - Returns a tuple with three elements. Middle one is the 
# word provieded as the argument. The first and last one are the other 
# characters around that word. In the rpartition case, the right most word 
# is considered. 

print('This is an apple is'.rsplit('is'))
# 43. .rsplit() - Returns a list, with the string splitted at the provided 
# arguments.

print('a', '       appple       '.rstrip(), 'b')
# 44. .rstrip() - Returns a right stripped version of the string. 

print('This is an apple is'.split('is'))
# 43. .split() - Returns a list, with the string splitted at the provided 
# arguments.

print('This is\nan apple'.splitlines())
# 44. .splitlines() - Returns a list with the elements by splitting the string 
# at line brakes. 

print('apple is a good fruit'.startswith('apple'))
# 45. .startswith() - Returns True if the string starts with a specific value.

print('a', '       b       '.strip(), 'c')
# 46. .strip() - Returns a stripped version of the string.

print('aBc'.swapcase())
# 47. .swapcase() - Returns a string with upper case letters changed to lower 
# case and lower case letters changed to upper case simultaneously.

print('The apple is good'.title())
# 48. .title() - Returns a string with all the words' first letters changed to 
# upper case. 

translation_table = str.maketrans('ab', 'cd', 'e')
print('abe'.translate(translation_table)) 
# 49. .translate() - Returns a translated string as per a specific translation
# table passed as argument. 

print('This is an apple'.upper())
# 50. .upper() - Returns a string with all the alphabets of the string 
# converted to upper case letters.

print('abc'.zfill(6))
# 51. zfill() - Returns a string with the beganing of the strin padded with 0
# as no of spaces mantioned per the agument 




































































# %%
