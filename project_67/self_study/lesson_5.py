# %%
"""This is lesson 5 and my 5th self study of python programming. 

Here it is explained all about the inbuilt exceptions in python that usually
occures in a plain C Python.
"""
# In Python when an exception occures we call it an error happened. 

# To understand what kind of things are defined as exeptions in Pyhton we need 
# to take a look of different exception first. Here's the list below.

# There are total 31 types of inbuilt exceptions in Python. 

# 1. ArithmeticError 

# 2. AssertionError

# 3. AttributeError 

# 4. Exception 

# 5. EOFError 

# 6. FloatingPointError

# 7. GeneratorExit 

# 8. ImportError 

# 9. IndentationError 

# 10. IndexError

# 11. KeyError 

# 12. KeyboardInterrupt

# 13. LookupError 

# 14. MemoryError 

# 15. NameError

# 16. NotImplementedError

# 17. OSError 

# 18. OverflowError

# 19. ReferenceError

# 20. RuntimeError

# 21. StopIteration

# 22. SyntaxError

# 23. SystemExit

# 24. TypeError

# 25. UnboundLocalError

# 26. UnicodeError 

# 27. UnicodeEncodeError 

# 28. UnicodeDecodeError 

# 29. UnicodeTranslateError 

# 30. ValueError

# 31. ZeroDivisionError

# Whenever a python exception occures, it shows the type of the exception and
# also the exception message, so there would not be any confusion on 
# understanding what the error is.  