# %%
"""This is lesson 4 and my 4th self study of python programming.

Here it is explained all the keywords in python.
"""
# There are 32 keywords in Python. 

if 1<2 and 3>2:
    print('This statement is true.')
# 1. and - This keyword means 'and', the way we use and in real life. 'and' is
# used in real life in statements to connect two different statements.

import numpy as np 
print(np)
# 2. as - To create alias. This thing is a little bit weird because you can 
# only use as when importing any module or it's components, opening any files.

# assert 1 > 2, '1 should be less than 2.'
# 3. assert - To check if a condition is true. If the condition returns false,
# assertion error is arrised and you can also attack an error message with it. 

i = 0
while i < 5:
    print('Value of i is', i)
    if i > 3:
        break
    print('Just a mark')
    i += 1
# 4. break - Used to immediately get out of the loop. It could lead to 
# confusion of if multiple loops are cascaded then which loop will break or the
# entire loop might break. The answer to this question is the moment the break
# statement is used, the just before loop breaks. But the main stream loops 
# continues to run.  

class MyClass:
    pass
my_object = MyClass()
print(my_object)
# 5. class - To define a class.

for i in range(0, 5):
    print('This is', str(i) + 'th iteration.')
    if i == 3:
        continue
    print('Now it is', i)
# 6. continue - discard the further lines of codes in the current iteration and
# go to the next iteration. This also acts like the break that it acts for the
# just before loop it has. 

def my_function():
    print('This is my own function.')
my_function()
# 7. def - To define a function. 

new_object = MyClass()
print(new_object)
del new_object
# print(new_object)
# 8. del - To delete an object. 

i = 5
if i > 5:
    print('i is greater than 5.')
elif i < 5:
    print('i is less then 5.')
else:
    print('i is equal to 5.')
# 9. elif - Means 'else if' as usded normally in the real life while 
# conditioning. 

if i > 5:
    print(i > 5)
else:
    print('anything other')
# 10. else - Means 'else' as used normally in the real life while conditioning.

try:
    x > 5
except:
    print('something went wrong.')
print('Program is stil runnning...')  
# 11. except - Always used with a try block, if the try block gives any error, 
# the except block executes and move on to the further lines of the code. This
# is done when a specific set of code might give error and might break the 
# entire execution of your code. In those case the would be error code set is 
# set in the try block and the if the error happen then the taken care part 
# is set on except block. 

if False:
    print('Wow')
# 12. False - The truth value, 'False', as used in real life. 

try:
    print(0/2)
except:
    print('An error occured.')
else:
    print('No error occured.')
finally:
    print('This is printed finally.')
# 13. finally - Always used with try, except block. The finally block is 
# executed finally whether any error is encountered by the try block or not. 
# try, except, else, finally can be used as a combinition of blocks to take 
# a proper control over error hading situations. 

for alphabet in ['a', 'b', 'c', 'f']:
    print(alphabet)
# 14. for - To create a for loop. 

from modules.the_depth_1.demo_module import AGE
print(AGE)
# 15. from - To import a perticular part of a module 

PLANET = 'Earth'
def my_funciton():
    global x, y, z, PLANET 
    x = 10
    y = 11 
    z = 12 
    PLANET = 'Mars'
my_funciton()
print(x, y, z, PLANET)
# 16. gobal - Used to define global variables inside a local scope (for 
# example defining a global variable inside a funciton). Whenever you want to 
# access a global variable from a local scope, modify it then declare the 
# variable with the global keyword first and then use it inside the local 
# scope to make a direct connection with the global variable scope. 

if 1 < 2:
    print('This is true.')
# 17. if - Used to execute a block of codes if a condition is true. 

import modules.the_depth_1.demo_module
print(demo_module.NAME)
# 18. import - Used to import a module. The path to the module should be 
# accompanied by dots up to the module itself. 

x = [1, 'a', 2, [2, 3]]
print('a' in x)
# 19. in - To check if a value is present in a sequential object. 

x = [2, 3]
y = [2, 3]
z = x
print(x is z)
# 20. is - ! Not fully cleared. This is used to check if two variables refers to the same object. 
# This does not mean if you do any change to one variable then the other one also 
# changes as they are refering to the same object. 

my_lambda_function = lambda x, y, z=3 : z + y + x 
print(my_lambda_function(1, 5)) 
# 21. lambda - Is used to create single expression funcitons. The funciton 
# would be assigned to a variable, you can pass as much arguments you want and
# would return a single object. 

print(None)
# 22. None - Represent a null value means nothing. 

def function_1():
    x = 'Keith'
    def function_2():
        nonlocal x
        x = 'David'
        def funciton_3():
            print('This is by function 3.')
        funciton_3()
    function_2()
    return x
print(function_1())
# 23. nonlocal - This is used to define variables in the inner funcitons in 
# the inner scope. This means the varial inside the inner scope would no 
# longer be treated as a local variable of the funciton. Rather it would be 
# effective to the outer funciton. ! The expert usage of this fucntion is not
# clear. 

if not False:
    print('This is not false.')
# 24. not - Means 'not' as used in real life. 

if 1>2 or 1==1:
    print('The statement is true.')
# 24. or - Means 'or' as used in real life. 

def my_funciton():
    pass 
# 25. pass - Means a null statement that would do nothing. Any starting of a 
# block (funcitons, for/while loops, classes etc) in python can't be empty. If
# you want to declare it without having any statements in it, you can use pass
# keyword to avoid that error. 

# x = 7
# if x > 5:
#     raise Exception('Not allowed')
# 26. raise - This is used to raise an error (errors allowed in python - for 
# example ValueError, Exception, ZeroDivisionError etc) with an error message.

def my_funciton():
    print('First line.')
    return 5
    print('Last line.')
print(my_funciton())
# 27. return - Used to return a value from a funciton and immediately stops
# the execution of the further lines of the funciton. 

if True:
    print('This is true.')
# 28. True - Means 'true' as used in real life. 

try:
    print(6/0)
except:
    print('Encountered an error.')
# 29. try - Used to set a block of code that might give error that would break
# the entire script. By using try blocks, the error won't stop the entire 
# execution of all the entire code and would skip to the except block to do 
# further processing and carry on the next codes. 

i = 1
while i < 5:
    print('Loop is running.')
    i += 1
# 30. while - To create while loops. 

with open('demofile.txt', 'r') as file_:
    print(file_.read())
# 31. with - To ensure enting to a stream and closing the stream properly 
# without explicitly declaring the closing stram statement to achieve a cleaner
# code that would result in less unwanted bugs. ! This keyword is not clear 
# yet.

def my_funciton():
    for i in range(0, 5):
        output = i + 5
        yield output
    print('The function execution is complete.')
values = my_funciton()
for value in values:
    print(value)
# 32. yield - To return multiple values from a fucniton without stoping the
# execution of the funciton as happens in return. In order to access the 
# the multiple returns of the function you need to loop over the output of the
# funciotn. 