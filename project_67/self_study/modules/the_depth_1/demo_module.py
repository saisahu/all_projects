# %%
AGE = 23
NAME = 'Sai Nilayam Sahu'

def my_funciton(a, b):
    return a + b

class MyClass:
    def __init__(self, name, age):
        self.name = name 
        self.age = age 

    def show(self):
        print('My name is', self.name, 'and age is', str(self.age) + '.')

my_object = MyClass(NAME, AGE)
my_object.show()
