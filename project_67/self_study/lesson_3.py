# %%
"""This is lesson 3 and my 3rd self study of Python programming. 

Here it is explained how to handle the files in python with all the methods
associated to the file object in python.
"""
# Before going to hadle files through python we need to get some ideas upon
# files. Here are the required concepts ragarding files. 

# Enocding
# --------
# A file in a computer is actually stored in terms of numbers. The encoding is 
# just an assignment of some perticular characters to some numbers. When 
# opening a file it shows the numbers stored in that file in terms of 
# characters as per the specified encoding.
# Examples of encodings are - UTF-8, ASCII etc.

# Buffering 
# ---------
# When you open a file through any program (it may be any text editor or any 
# programming language), the operation system starts a process to take control
# over the file in order to do some operations (read, write, setting the 
# pointer, save, delete the file, delete the a perticular part of the file etc.
# ) upon it. This process is called buffering. 

# File Handiling
# -------------- 
# This is the set of different operations that could be executed upon a file. 
# These are - 
# 1. Instatiating the buffer for a file.
# 2. Setting the encoding of the file. 
# 3. Creating a file. 
# 4. Setting the pointer inside the file.
# 5. Reading the file. 
# 6. Writting in the file. 
# 7. Saving the file. 
# 8. Deleting the file. 
# 9. Deleting anything inside the file. 
# 10. Closing the buffer. 

# Now all the tutorial following is all about how to do all these thing using
# python.

# Python all file methods


# file_ = open('demofile.txt', 'r')
# print(file_)
# print(file_.read())
# A File can be opened in python with different modes namely - read, write,
# append, create. The open function actually maps a real file to a built in 
# python file object.

# file_.close()
# print(file_)

# 1. .close() - Closes an opened file buffer. This is done due to sometimes 
# bufferening, changes made to a file may not show, so it is important to close
# a file at the end after any changes made to it. 

# print(file_.detach())
# 2. .detach() - ! This method is not clear yet.

# print(file_.fileno())
# 3. .fineno() - ! This method is not clear yet.

# print('Before flush' + '\n' + file_.read())
# file_.flush()
# print('After flush' + '\n' + file_.read())
# 4. .flush() - Removes all the contents of the internal buffer of the file
# without really changing anything to the file. 

# print(file_.isatty())
# 5. .isatty() - ! This methos is not clear yet. 

# print(file_.read())
# 6. .read() - Returns the content of the file. 

# file_ = open('demofile.txt', 'r')
# print(file_.readable())
# 7. .readable() - Returns True is the file stream is readable. 

# print(file_.readline(), end='')
# print(file_.readline())
# 8. .readline() - Returns one after another line from the file whenever it is
# called. This means when this method is called for first time it will return
# the first line, when called for 2nd time, it will return the second line and
# so on. 

# print(file_.readlines())
# 9. .readlines() - Returns a list of all the lines in a file. It breaks all 
# the content of the file at all the line break characters and returns a list
# of all the parts. 

# print(file_.read())
# file_.seek(5)
# print(file_.read())
# 10. .seek() - Changes the position of the pointer of the file.

# print(file_.seekable()) 
# 11. .seekable() - Returns True if the file object allows to change the 
# pointer's position.

# file_.seek(6)
# print(file_.tell())
# 12. .tell() - Returns the current pointer's position in the file object. 

# file_ = open('demofile.txt', 'w')
# file_.write(
#     'This is the first line of the file.' 
#     '\nThis is the second line of the file.'
# )
# file_.truncate(10)
# file_ = open('demofile.txt', 'r')
# print(file_.read())
# 13. .truncate() - Removes all the elements of the file after a specified 
# position i.e limits the characters of the file upto a specified position. 

file_ = open('demofile.txt', 'w')
print(file_.writable())
# 14. .writable() - Returns True if the file object is writable. 

file_.write('This is written using python.')
with open('demofile.txt', 'r') as file_:
    print(file_.read())
# 15. .write() - Writes to the current file opened. 

with open('demofile.txt', 'w') as file_:
    file_.writelines(['This is a line', 'The second line', '\n', 'some texts'])
with open('demofile.txt', 'r') as file_:
    print(file_.read())
# 16. .writelines() - Writes a list of strings to the file.























