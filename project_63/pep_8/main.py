
# import this

"""this is a demo for pip8"""

# Never use l, O and I as a single letter varibale name as
# this may be wrongly taken as 1.
# 
# You should always take care of this thing.
l = 0   # Never do this
O = 0
I = 0

"""Demo function for pip8 python
writting. This is always a good practice writting them for all public 
modules, classes, functions, methods, attributes.
"""


def function(var_1, var_2, var_3,
             var_4, var_5):
    a = 1
    b = 2
    c = 3

    mul = a * b * c
    output = mul * mul

    return output


def my_function(a, b, c, d, e):
    return 0

if 1<2 and 2<4 and 3==3 and \
   4==4:
    # If the condition satisfied
    print('done')

object_new = my_function(
        a='apple', b='ball', c=5,
        d='cat', e=[1, 2, 3])

print(1 + 2 + 3 + 4 + 6
      + 7 - 8 - 9 -10 -11 -12
      + 13)   

# Any name should always be most consine but descriptive
def multiply_by_two(x):
    return x * 2


x = 1
var = 2
my_variable = 3
name = 'Sai Sahu' 
first_name, last_name = name.split(' ')
print(first_name, last_name, sep=', ')

lst = [
    1, 2, 3, 4, 5, 6,
    7, 8, 9, 10, 11,
    12, 13
]

class Model:
    def method(self):
        a = 1
        b = 2

        add = a + b
        output = add * 10

        return output

    def class_method(self):
        pass


class MyClass:
    pass


C = 1
CONSTANT = 2
CONSTANT_NBR = 3

import module
import my_module

import package
from package.mypackage import module_4

# Using white sparsing in different places of python
a, b, c = 10, 11, 12
b <= 15
c += 1

if a and b or c:
    pass

# It is recommended to only add white space around the operator 
# that has lowest priority in a multi operator operation

e = a*5 + 5*6
f = 6/9*5+4 - 6*8

if a/8*3>9 and (1-5*c):
    pass

lst[3 : 4+a]

# When to avoid white space

# Never use a white space at the end of a line. If it gives
# any error then it's hard to trace.

# Also there should not be any trailling white spaces anywhere inside your cod
# e.

arr = [1, 2, 3, 4, 5]
tpl = (1, 2, 3, 4, 5)
dic = {'a':1, 'b':5}

my_function(1, 2, 3, 4, 5)

lst[3]
g = (5,)







