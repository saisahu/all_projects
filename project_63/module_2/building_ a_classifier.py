# %%
"""Building a classifier."""

"""Creating a CSV file with all the feature urls, labels and class names."""
# Now let's make a featuer label format.
# A python array that will store all of my fetures i.e. the url of the images.
# A python array that will store all the labels.
# A python array that will store all the respective class name.
features_url = []
labels = [] 
class_name = []==

import os

# Get all the images name from the dir.
dirs_a = os.listdir('../data/for_image_recognition/a')
# Create all the url and append it to features_url and also assign the label 
# to the labesl array.
for i in dirs_a:
    inst_url = '../data/for_image_recognition/a/' + i
    # print(inst_url)
    features_url.append(inst_url)
    labels.append(1)
    class_name.append('a')

# Doing the same thing for all the images of b (class-2)
dirs_b = os.listdir('../data/for_image_recognition/b')
for i in dirs_b:
    inst_url = '../data/for_image_recognition/b/' + i
    features_url.append(inst_url)
    labels.append(2)
    class_name.append('b')

# Doing the same thing for all the images of b (class-2)
dirs_c = os.listdir('../data/for_image_recognition/c')
for i in dirs_c:
    inst_url = '../data/for_image_recognition/c/' + i
    features_url.append(inst_url)
    labels.append(3)
    class_name.append('c')


#  %%

# Create a data frame from the above three arrays.
import pandas as pd 
list_of_tuples = list(zip(features_url, labels, class_name))                   
df_feature_label_class = pd.DataFrame(
    list_of_tuples,
    columns=['Feature_URLs', 'Label', 'Class_Name']
)


# %%
# Save it to a csv file.
pd.set_option('display.max_rows', 10)
display(df_feature_label_class)
df_feature_label_class.to_csv('df_feature_label_class.csv')

# %%
"""Preparing the feature label data set."""
# Read the data from the feature_label_class csv file.
df_feature_label_class = pd.read_csv('df_feature_label_class.csv')
display(df_feature_label_class)


# %%
# Collecting all the images to a python list
features = []
labels = df_feature_label_class['Label']

# But as per Keras, the labels should be 0 indexed, so not 1, 2, 3. It should
# be 0, 1, 2
labels_0_indexed = []
for i in labels:
    labels_0_indexed.append(i-1)

# Getting the pandas series of 'Feature_URLs' column.
feature_urls = df_feature_label_class['Feature_URLs']

# Import all the image from the urls
import cv2

for i in feature_urls:
    inst_img = cv2.imread(i)
    # Resize the instant image to 50x50 px
    inst_img_resized = cv2.resize(inst_img, (50, 50))
    # Convert the image to grey scale
    inst_img_grayed = cv2.cvtColor(inst_img_resized, cv2.COLOR_BGR2GRAY)

    features.append(inst_img_grayed)

# Converting the features and label_0_indexed arrays to numpy arrray
import numpy as np

features = np.array(features)
labels_0_indexed = np.array(labels_0_indexed)


# %%
# Shape of features is 50X50 px pictures and a total of 45 pictures so 
# 45*(50*50)
print('Shape of features:', features.shape)
print('Shape of labels:', labels.shape)


# %%
# Now flattenout our features to so that 50X50 images will be 2500 features 
# and the shape of our features will be (45, 2500)
features_flatten = features.reshape(45, 2500)
print('shape of the features_flatten:', features_flatten.shape)

# Now scale our features
from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler(feature_range=(0, 1))
scaler.fit(features_flatten)
features_flatten_scaled = scaler.transform(features_flatten)
# print(features_flatten_scaled)

# Now save the scaler model for usage over new data.
from pickle import dump
dump(scaler, open('scaler.pkl', 'wb'))


# %% 
# Now let's build the Neural Network Architecture
import tensorflow as tf 
from tensorflow import keras 
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Activation, Dense
from tensorflow.keras.optimizers import Adam 
from tensorflow.keras.metrics import CategoricalCrossentropy 

# Building the network
model = Sequential([
    # As the features_flatten_scaled is of shape (45, 2500), this means
    # out network will take input of 2500 features a time for a single 
    # training example. So the input shape is gonna be (2500,). Here a comma is
    # necessary as per shaping convension.
    Dense(units=30, input_shape=(2500,), activation='relu'),
    Dense(units=30, activation='relu'),
    Dense(units=3, activation='softmax')
])

# Get the model summery
model.summary()

# Define how the model will be compiled(how control of the optimization flow)
model.compile(
    optimizer=Adam(learning_rate=0.0001),
    loss='sparse_categorical_crossentropy',
    metrics = ['accuracy']
)

# %%
# It's necessary to split shuffle our data before feeding it into our model.
# As the cross validation (validation) set is separated before shuffling while
# training out model.
from sklearn.utils import shuffle
features_flatten_scaled , labels_0_indexed = shuffle(
    features_flatten_scaled, labels_0_indexed
)


# %%
# Now lets train the our Neural Network
model.fit(
    x=features_flatten_scaled,
    y=labels_0_indexed,
    batch_size=10,
    epochs=100,
    shuffle=True,
    verbose=2, 
    # 0.1 of the training data will be separated out for validation set and 
    # not included during training, but remember that the splitting happens 
    # first then the shuffle, so it is recommended that you need to shuffle 
    # all your training set before you pass it to the model.
    validation_split=0.6
)

# %%
# Now lets save our model
model.save('image_recognition_model')

# %%
# Importing back our saved model
from tensorflow.keras.models import load_model
model_trained = load_model('image_recognition_model')

# %%
# Get the model summery  
model_trained.summary()


#  %%
# Take new test data and process it through the same pipeline
# For this testing perpose and for prediction of new data follow
# another module, face recognition apply


# %%
# For Rough work
