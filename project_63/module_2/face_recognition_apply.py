# %%
"""Apply face recognition"""
# Get the folder name
# Take the folder name as command line argument
# import sys
# folder_name = sys.argv[1]

# Take the folder name as input
folder_name = input(
    'Enter the path to the folder where the images are stored:'
)

# Folder name for testing perpose
# folder_name = 'new_faces'

# Get the list of all foleders
import os
dirs = os.listdir(folder_name)
# print(dirs)

# Get all features (images in 50X50 px in grayscale) in the feature list 
features = []

import cv2
for i in dirs:
    inst_url = folder_name + '/' + i
    inst_img = cv2.imread(inst_url)
    inst_img_resized = cv2.resize(inst_img, (50, 50))
    inst_img_grayed = cv2.cvtColor(inst_img_resized, cv2.COLOR_BGR2GRAY)
    features.append(inst_img_grayed)

# Convert the features list to numpy array
import numpy as np
features = np.array(features)
print('Shape of features:',features.shape)

no_of_imgs = len(features)
# Flatten the features
features_flatten = features.reshape(no_of_imgs, 2500)
print('Shape of feature flatten:', features_flatten.shape)

# Now lets import our scaler model and scale our flatten features
from pickle import load
scaler = load(open('scaler.pkl', 'rb'))
features_flatten_scaled = scaler.transform(features_flatten)

# Now import our Neural Network model
from tensorflow.keras.models import load_model
model_trained = load_model('image_recognition_model')
# Get summary of the model
print('\nSummary of the trained model:')
model_trained.summary()

# Do predictions using our network
predictions = model_trained.predict(features_flatten_scaled)
print('\nProbabilities given by model:')
print(predictions)

# Get the index of the max probalities of our model predictions, the labels.
labels = np.argmax(predictions, axis=-1)
print('\nPredicted Labels:')
print(labels)

# Getting the predicted classes
classes = ['a', 'b', 'c']
predicted_classes = []

for i in labels:
    predicted_classes.append(classes[i])

print('\nPredicted Classed:')
print(predicted_classes)

# Printing out the name of the images to cross verify.
print('\nNames of the images')
print(dirs)





